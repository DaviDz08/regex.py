import re

palabra = input("Palabra a Evaluar: ")

if re.match('[A-Z][a-z]+\s[A-Z](\.|[a-z]*)', palabra):
    print("Coincidencia")
else:
    print("Sin coincidencia")
# (\.|[a-z]*)  o algo que empiece con . o una letra de la a-z
#-----------------------------------------------------------------------------------------------
#[A-Z] [a-z]+ (asentl sircunflejo, al inicio)   refleja que lo que esta a su mano derecha, lo obliga que este al inicio
#\.(com)$ tiene que terminar con com
#----------------------Cuaderno de apuntes-------------------------------------------------------------------------------
#  1  import re
#  2
#  3 palabra = input("Palabra a Evaluar: ")
#  4
#  5 if re.match('.asa', palabra):
#  6     print("Coincidencia")
#  7 else:
#  8     print("Sin coincidencia")
#casa buscara cualquiera palabra que empieze con esto
#El . es un comodin
#el \. aqui evalua que empieza con "." (Punto)
#-----------------------------------------------------------------
#  1 import re
#  2
#  3 palabra = input("Palabra a Evaluar: ")
#  4
#  5 if re.match('jpg|png|gif', palabra):
#  6     print("Coincidencia")
#  7 else:
#  8     print("Sin coincidencia")
#-------------------------------------------------------------------
#  1 import re
#  2
#  3 palabra = input("Palabra a Evaluar: ")
#  4
#  5 if re.match('\.(jpg|png|gif)', palabra):
#  6     print("Coincidencia")
#  7 else:
#  8     print("Sin coincidencia")
#----------------------------------------------------------------------
#  1 import re
#  2
#  3 palabra = input("Palabra a Evaluar: ")
#  4
#  5 if re.match('ca(--|...)ta', palabra):
#  6     print("Coincidencia")
#  7 else:
#  8     print("Sin coincidencia")
#-------------------------Rango de los numeros------------------------------------------------
#  1 import re
#  2
#  3 palabra = input("Palabra a Evaluar: ")
#  4
#  5 if re.match('R[0-9]D[0-9]', palabra):
#  6     print("Coincidencia")
#  7 else:
#  8     print("Sin coincidencia")
#--------------------------puede poner cualquier letra de la "a" a la "z"------------------------------------------------
#  1 import re
#  2
#  3 palabra = input("Palabra a Evaluar: ")
#  4
#  5 if re.match('ca[a-z]a', palabra):
#  6     print("Coincidencia")
#  7 else:
#  8     print("Sin coincidencia")
#----------------------------------------------------------------------
#  1 import re
#  2
#  3 palabra = input("Palabra a Evaluar: ")
#  4
#  5 if re.match('ca[a-c0-5]a', palabra):
#  6     print("Coincidencia")
#  7 else:
#  8     print("Sin coincidencia")
#
#-----------------------------------------------------------------------
#  1 import re
#  2 
#  3 palabra = input("Palabra a Evaluar: ")
#  4 
#  5 if re.match('ca[a-c0-5]a', palabra):
#  6     print("Coincidencia")
#  7 else:
#  8     print("Sin coincidencia")
#
#---------------------------------------------------------------------
#  1 import re
#  2 
#  3 palabra = input("Palabra a Evaluar: ")
#  4 
#  5 if re.match('ca\da', palabra):
#  6     print("Coincidencia")
#  7 else:
#  8     print("Sin coincidencia")
#---------------------------------------------------------------------
#  1 import re
#  2 
#  3 palabra = input("Palabra a Evaluar: ")
#  4 
#  5 if re.match('ca.a\w', palabra):
#  6     print("Coincidencia")
#  7 else:
#  8     print("Sin coincidencia")
#  9 
#---------------------------------------------------------------------
#  1 import re
#  2 
#  3 palabra = input("Palabra a Evaluar: ")
#  4 
#  5 if re.match('ca\ssa', palabra):
#  6     print("Coincidencia")
#  7 else:
#  8     print("Sin coincidencia")
#
#---------------------------------------------------------------------
#  1 import re
#  2 
#  3 palabra = input("Palabra a Evaluar: ")
#  4 
#  5 if re.match('[A-Z][a-z]+\s[A-Z](\.|[a-z]*)', palabra):
#  6     print("Coincidencia")
#  7 else:
#  8     print("Sin coincidencia")
#  9 # (\.|[a-z]*)  o algo que empiece con . o una letra de la a-z
#--------------------------------------------------------
#  1 import re
#  2
#  3 palabra = input("Palabra a Evaluar: ")
#  4
#  5 if re.search('[A-Z][a-z]+\s[A-Z](\.|[a-z]*)', palabra):
#  6     print("Coincidencia")
#  7 else:
#  8     print("Sin coincidencia")
#  9 # (\.|[a-z]*)  o algo que empiece con . o una letra de la a-z
#permite desde donde sea la expresion
#-----------------------------------------------------------------------
#\d que el caracter sea un digito
#\D que el caracter no sea un digito
#\w que sea un caracter alfa numerico
#\W que no sea un caracater alfa numerico
#\s que sea un espacio en blanco
#\S que sea lo que sea menos un espacio en blanco
#----------------------------------------------------------------------
#+ lo que esta a la izquierda va aparecer 1 o varias veces
#* es 0 o varias 
#? 0 o 1
#{} numero de veces que se van a repetir
#---------------------------------------------------------------------